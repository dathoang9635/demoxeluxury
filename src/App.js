import BaiTapChonXe from "./BaiTapChonXe/BaiTapChonXe";

/*
. import css có syntax đơn giản hơn imprt các class 
. bỏ  file css và js chung file để dễ quả lý hơn
.

*/
function App() {
 return (
  <div className="App">
   {/* <State /> */}
   {/* <Styling /> */}

   <BaiTapChonXe />
  </div>
 );
}

export default App;
