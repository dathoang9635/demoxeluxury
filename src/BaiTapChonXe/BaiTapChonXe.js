/*
 . Load hình sử dụng require có sử dụng default ở cuối.
 . việc cài đặt state: không rườm rà như thao tác ở js thuần, hoàn toàn phải dom tới html nào cần thay đổi, với state chỉ cần lựa chọn THUỘC TÍNH đối tượng cần thay đổi


*/
import React, { Component } from "react";

export default class BaiTapChonXe extends Component {
 state = {
  imgProduct: require("../asset/products/black-car.jpg").default
 };

 renderCar = (imgNewProduct) => {
  //   let newState = {
  //    imgProduct: imgNewProduct
  //   };

  this.setState({ imgProduct: imgNewProduct });
  //   có thể sử dụng cách viết tắt như thế này trong set state
 };

 render() {
  return (
   <div>
    <div className="container">
     <div className="row">
      <div className="col-7">
       <img
        className="img-fluid"
        src={require("../asset/products/black-car.jpg").default}
        alt="blackcar"
       />
      </div>

      <div className="col-5">
       <div className="card text-white ">
        <div className="card-header text-primary font-weight-bold">
         Exterior color
        </div>

        <div className="card-body">
         {/* blackcar */}
         <div
          onClick={() => {
           console.log(123);
           this.renderCar(require("../asset/products/black-car.jpg").default);
          }}
          style={{ cursor: "pointer" }}
          className="row border border-light py-2"
         >
          <img
           className="col-2 img-fluid"
           src={require("../asset/icons/icon-black.jpg").default}
           alt="blackIcon"
          />
          <div className="col-10 text-dark">
           <h3 className="font-wight-bold m-0">Crystal black</h3>
           <p className=" m-0">Pearl</p>
          </div>
         </div>
         {/* red car */}
         <div
          className="row border border-light py-2"
          onClick={() => {
           console.log(456);
           this.renderCar(require("../asset/products/red-car.jpg").default);
          }}
          style={{ cursor: "pointer" }}
         >
          <img
           className="col-2 img-fluid"
           src={require("../asset/icons/icon-red.jpg").default}
           alt="redIcon"
          />
          <div className="col-10 text-dark">
           <h3 className="font-wight-bold m-0">Crystal black</h3>
           <p className=" m-0">Pearl</p>
          </div>
         </div>
         {/* silver car */}
         <div
          className="row border border-light py-2"
          onClick={() => {
           console.log(789);
           this.renderCar(require("../asset/products/silver-car.jpg").default);
          }}
          style={{ cursor: "pointer" }}
         >
          <img
           className="col-2 img-fluid"
           src={require("../asset/icons/icon-silver.jpg").default}
           alt="silverIcon"
          />
          <div className="col-10 text-dark">
           <h3 className="font-wight-bold m-0">Crystal black</h3>
           <p className=" m-0">Pearl</p>
          </div>
         </div>
         {/* steel */}
         <div
          className="row border border-light py-2"
          onClick={() => {
           console.log(101);
           this.renderCar(require("../asset/products/steel-car.jpg").default);
          }}
          style={{ cursor: "pointer" }}
         >
          <img
           className="col-2 img-fluid"
           src={require("../asset/icons/icon-steel.jpg").default}
           alt="steelIcon"
          />
          <div className="col-10 text-dark">
           <h3 className="font-wight-bold m-0">Crystal black</h3>
           <p className=" m-0">Pearl</p>
          </div>
         </div>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
  );
 }
}
