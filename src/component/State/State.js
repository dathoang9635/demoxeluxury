import React, { Component } from "react";

export default class State extends Component {
 /*
. setState là phương thức có sẵn, giúp người dùng có thể,
. Lưu ý hàm State và setStatus
. state là thuộc tính có sẵn của react class component giúp định nghĩa những nội dung có khả năng thay đổi khi người dùng thao tác sư kiện, sẽ tự động render lại sự kiện.
 */
 state = {
  status: false
 };

 userLogin = {
  name: "hoang Dat",
  age: 12
 };

 login = () => {
  /*
 . nếu gọi theo phương thức cũ thì dù cho có gọi lại hàm this.render thì nó cũng không run
 . this.state.status = true; không set giá trị trực tiếp trên state, phải thông qua phương thức setState,
 . set state là 1 phương thức bất đồng bộ, các câu lệnh phía sau có khả năng chạy mà không cần nó phản hồi
 . 
  */

  let newState = {
   status: true
  };

  //  syntax lạ, không truyền vào một tham số trực tiếp, mà truyền vào tham số có sự giống nhau về cấu trúc thuộc tính

  this.setState(newState);
 };

 renderUserLogin = () => {
  if (this.state.status) {
   return <div className="text-align-center">{this.userLogin.name}</div>;
  }
  return (
   <button
    onClick={() => {
     this.login();
    }}
   >
    Dang nhap
   </button>
  );
 };

 render() {
  return <div>{this.renderUserLogin()}</div>;
 }
}
