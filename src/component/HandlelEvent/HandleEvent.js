import React, { Component } from "react";

export default class HandleEvent extends Component {
 // định nghĩa xử lý sự kiện
 //  Lưu ý chính tả của sự kiện, camelLetter
 handleClick = () => {
  alert(123);
 };
 render() {
  return (
   <div>
    <button onClick={this.handleClick}>Click here</button>
   </div>
  );
 }
}
