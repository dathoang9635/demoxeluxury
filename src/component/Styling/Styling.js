/*
. tính đề xuất trong jsx thực sự không mạnh.
. sử dụng cách import module để có thể set thuộc tính css
. 
.
.
*/
import React, { Component } from "react";

import Child from "./Child";

import "./Styling.css";

import style from "./Styling.module.css";

export default class Styling extends Component {
 render() {
  return (
   <div>
    <p className="txt">helloo</p>
    {/* cái này mới nhận code */}
    <p className={style.txtStyle}>Xin chao</p>
    {/* cái này hoàn toàn không nhận code */}
    <p className=" txtStyle"> baba</p>
    <Child />
   </div>
  );
 }
}
