import React, { Component } from "react";

export default class DataBinding extends Component {
 //  tạo 1 thuộc tính, và sử dụng lại thuộc tính đó thì sử dụng this, bỏ trong dấu {}, hay còn gọi là kỹ thuật binding
 //  cái này giống với việc sử dụng es6
 name = "Phan Hoàng Đạt";

 // tạo 1 object và binding 1 object
 student = {
  tên: "Hoàng Đạt",
  tuổi: 12,
 };

 //  tạo 1 phương thức, tất cả các hàm đều phải trả về một giá trị, thẻ img có vẻ giống với thẻ html nhưng nó thực sự là một đối tượng, lúc gọi this nhớ sử dụng dấu ngoặc để gọi hàm
 renderimg = () => {
  return (
   <img
    src="https://kenh14cdn.com/2017/000001-1483596124398.jpg"
    alt="kimbokjoo"
   />
  );
 };

 //  const virus = {
 // 	id: "covid",
 // 	age: 2,
 // 	name: "corora",
 //    };
 render() {
  //  tạo 1 biến, sử dụng biến sử dụng trong dấu {}
  const school = "Can Tho";

  return (
   <div>
    {/* <h1>
     {this.name} - school : {school}, tuổi - {this.student.tuổi}
    </h1> */}
    <h3>Bindinging function</h3>
    {this.renderimg()}
   </div>
  );
 }
}
